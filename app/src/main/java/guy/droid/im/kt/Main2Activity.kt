package guy.droid.im.kt

import android.content.DialogInterface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.properties.Delegates

class Main2Activity : AppCompatActivity() {



    val KOTLIN = "KOTLIN"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

    //    Add(2,5)
    //    RetrunDisp(AddReturn(5,5).toString())

    //    immutables()
    //    mutables()
    //    Stemplates()
    //      ConditionS()
    //     NullCheck()
    //    NullCheck2()
    //       NullCheck3()
    //    NullCheck4()
   //     TypeCast()
   //       Classes()
   //     WhenFunction()
   //     ForLoops()
    //    Operands()
    //    Looper()
    //    ClassCall()
     //     GetterAndSetter()
     //   DelegatesProperties()
        Interop()
    }



    /**     function in kotlin      **/
    fun Add(a :Int,b :Int)
    {


        Log.d(KOTLIN,"$a add $b is ${a+b}")

    }
    fun AddReturn(a :Int,b :Int) : Int
    {

        return a+b
        Log.d(KOTLIN,"$a add $b is ${a+b}")

    }

    fun RetrunDisp(disp : String)
    {
        Log.d(KOTLIN,disp)
    }
    /**     function in kotlin  end  **/

    /**     variables in kotlin    **/

    fun immutables()
    {
        val i : Int = 5
       //  i++ Cannot Be Used
        Log.d(KOTLIN,i.toString())
        val j = 100
        Log.d(KOTLIN,j.toString())
    }
    fun mutables()
    {
        var i = 5
        i = ++i
        Log.d(KOTLIN,i.toString())

        var j : Int = 100
        j = j+j
        Log.d(KOTLIN,j.toString())
    }
    /**     variables in kotlin   end **/

    /**     String Templates in kotlin    **/
    fun Stemplates()
    {
        var a = 5;
        val S1 : String =  "value of a is $a"
        Log.d(KOTLIN,S1)
        a = a * a
        var S2 = "${S1.replace("is","was")} but now $a"
        Log.d(KOTLIN,S2)
    }
    /**     String Templates in kotlin   end **/

    /**     Conditional Expression in kotlin   **/
    fun ConditionS()
    {
       // ConditiIF(5,6)
        Log.d(KOTLIN,ConditiIF(5,6).toString())
    }
    fun ConditiIF(a :Int,b :Int) :Int
    {
        if(a>b)

            return a
        else
            return b

    }
    /**     Conditional Expression in kotlin   end**/

    /**     Null Check in kotlin  **/
    fun NullCheck() {

       var a = "SASD";
        if(a == null)
        {
            Log.d(KOTLIN,(a?.length).toString())
        }else
        {
            Log.d(KOTLIN,(a?.length).toString())
        }
        var listitems : List<String?> = listOf("RAZ",null,"RAZZ",null)
        for(item in listitems)
        {
            item?.let { Log.d(KOTLIN,item) } // Displays only items with Values

        }

    }

    fun NullCheck2()
    {


        var listitems : List<String?> = listOf("RAZ",null,"RAZZ",null)
        for(item in listitems)
        {
            //item?.let { Log.d(KOTLIN,item) } // Displays only items with Values

            var res = item ?: "some"  // return -1 or some in case of null
            Log.d(KOTLIN,res.toString())
        }
    }
    fun NullCheck3()
    {
        var b : String = "";
        Log.d(KOTLIN,(b!!.length).toString())

    }
    fun NullCheck4()
    {
        val nullableList: List<Int?> = listOf(1, 2, null, 4)
        val intList: List<Int> = nullableList.filterNotNull()
        for(item in intList)
        {
            Log.d(KOTLIN,item.toString())
        }

    }
    /**     Type Cast in kotlin  **/

    fun TypeCast()
    {
        val someList: List<Any?> = listOf(1, 2, null, 4, "55","RAZ");
        for(item in someList)
        {
            item.let { if (item is String) Log.d(KOTLIN,"$item is String") else if ( item is Int) Log.d(KOTLIN,"$item is Int") else  Log.d(KOTLIN,"$item is SOMETHING ELSE")}
        }
    }
    /**     Type Cast in kotlin  end**/

    /**     Classes & Type Cast in kotlin  **/

    fun Classes()
    {
        val items = listOf("apple", "banana", "kiwi")
        for (item in items) {
            Log.d(KOTLIN,"$item")
        }

        val items2 = listOf("apple", "banana", "kiwi")
        for (index in items2.indices) {
            Log.d(KOTLIN,"item at $index is ${items2[index]}")

        }
    }
    /**     Classes & Type Cast in kotlin  end**/

    /**     Function when & For in kotlin  **/
    fun WhenFunction()
    {
        for(items in 1..10)
        {
           // Log.d(KOTLIN,"$items")
            when(items)
            {

               // 2,4,6,8 -> Log.d(KOTLIN,"$items is EVEN")


                in 1..5 -> Log.d(KOTLIN,"$items in 5")
                in 5..10 -> Log.d(KOTLIN,"$items out 5")

               // else -> Log.d(KOTLIN,"$items is  ODD")
            }
        }
    }

    fun ForLoops()
    {
        for(items in 1..10)
        {
            Log.d(KOTLIN,"$items - in F1")
        }

        var num_array : List<Any> = listOf(1,2,32,4,5)
        for(items in num_array)
        {
            Log.d(KOTLIN,"$items - in F2")
        }
        for (items: Int in num_array.indices) {
            Log.d(KOTLIN,"$items - contains Item ${num_array[items]} in F3")
        }

        for ((index, value) in num_array.withIndex()) {

            Log.d(KOTLIN,"the element at $index is $value in F4")
        }
    }


    /**     Function when  & For in kotlin end **/
    /**     Operands in kotlin **/
    fun Operands()
    {
        var x = (10 or 4)
        Log.d(KOTLIN,"$x")
    }
    /**     Operands in kotlin end**/

    /**     Loop Contine break in kotlin **/
    fun Looper ()
    {
        looped@ for(items in 1..10)
        {

            for (itemss in 1..10)
            {
                Log.d(KOTLIN," $items - ${itemss}")
                if (itemss == 8) {
                    continue@looped
                }

            }
        }

        var arrays : List<Any> = listOf(1,2,3,4,5)
        arrays.forEach lited@{
            if(it == 3) { return@lited }
            else
            {
                Log.d(KOTLIN," LOOP ONE $it")
            }

        }
        arrays.forEach {
            if(it == 3) { return@forEach }
            else
            {
                Log.d(KOTLIN," LOOP TWO $it")
            }

        }
        arrays.forEach {
            if(it == 3) { return }
            else
            {
                Log.d(KOTLIN," LOOP THREE $it") // GETS TERMINATED FROM THE LOOP
            }

        }
    }

    /**     Loop Contine break in kotlin End**/

    /** function class function start **/
    fun ClassCall()
    {
      var person : PersonKT = PersonKT()
      person.DefaultCaller()
      var result : Int = person.Add(10,55)
        Log.d(KOTLIN,"MAIN AFTER ADD $result")
        person.absfunction("RAZ")
        var person2 : PersonKT2 = PersonKT2("RAZ",900)
        person2.Displayer()
        var result2 = person2.Add(100,99)
        Log.d(KOTLIN,"MAIN AFTER ADD SECOND $result2")
        var result3 = person2.Adders(100,50)
        Log.d(KOTLIN,"MAIN AFTER ADD ADDERS $result3")
    }

    /** function class function start **/

    /** Properties in Kotlin Start **/
    // Getter and Setters
    fun GetterAndSetter()
    {
        var person : Person = Person()
        person.names = "RAZ"
        person.id = 1
        person.department = "CSE"

        var items = mutableListOf<String>()
        items.add("coins")
        items.add("dollars")
        var arraystosend : List<String> ? = items.toList()
        person.hobs = arraystosend

        var hash = mutableMapOf<String,String>()
        hash.put("ONE","1")
        hash.put("TWO","2")
        person.hashmap = HashMap(hash)


        var person2 : PersonKT = PersonKT()
        person2.DetailsViewer(person)
    }
    /** Properties in Kotlin End   **/

    /** Delefate Properties in Kotlin **/
    fun DelegatesProperties()
    {

        Log.d(KOTLIN,"LAZY VALUES $LazyValue")
        name = "RAZIK"
        name = "MOHAMED RASHIK"

        /* Storing Properties in a Map */

        val user  = Mapped(mapOf(
                "names" to "RAZ",
                "idd" to 1,
                "named" to 133,
                "TESTED" to "asd"

        ))
        Log.d(KOTLIN,"Storing Properties in a Map  ${user.names} ${user.idd} ${user.named} ${user.TESTED} ")
    }
    val LazyValue: String by lazy {
        "SOME STRING"

    }
    var name: String by Delegates.observable("") {
        prop, old, new -> SameTrigger(old,new)

    }
    fun SameTrigger(old : String,new : String)
    {
        Log.d(KOTLIN,"SAME TRIGGER $old CHANGED TO $new")
    }


    /** Delefate Properties in Kotlin End**/

    /** INTEROP **/
    // CALLING A JAVA FUNCTION FROM KOTLIN
    fun Interop()
    {
        var JString : List<String> ?
        var products : Products = Products()
        JString = products.returner()
        for(item in JString)
        {
            Log.d(KOTLIN,"JAVA FUNCTION $item")
        }
    }

    /** INTEROP **/
}
