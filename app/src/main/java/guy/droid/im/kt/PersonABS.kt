package guy.droid.im.kt

import android.util.Log

/**
 * Created by admin on 6/28/2017.
 */
abstract class PersonABS {
    val KOTLIN = "KOTLIN"
    init {

    }

    fun absfunction(name : String)
    {
        Log.d(KOTLIN,"ABSTRACT $name")
    }
}