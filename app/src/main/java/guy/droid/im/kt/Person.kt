package guy.droid.im.kt

/**
 * Created by admin on 6/28/2017.
 */
class Person
{

        var id: Int? = null
        get() = field
        set(value) { field = value }

        var names: String = ""
        get() = field
        set(value) { field = value }

        var department: String? = ""
        get() = field
        set(value) { field = value }

        var hobs : List<String>? = null
        get() = field
        set(value) { field = value }

        var hashmap : HashMap<String,String>? = null
        get() = field
        set(value) { field = value }
}