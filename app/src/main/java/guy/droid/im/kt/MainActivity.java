
package guy.droid.im.kt;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;

import kotlin.jvm.JvmName;

public class MainActivity extends AppCompatActivity {
    private static final String KOTLIN = "KOTLIN";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ProductsK productsK = new ProductsK();
        ArrayList<String> fromkotlin = new ArrayList<>();
        fromkotlin = productsK.GetMe();
        Log.d(KOTLIN, String.valueOf(fromkotlin.size()));

        JK jk = new JK();
        ArrayList<String> fromkotlin2  = jk.GetMe();
        Log.d(KOTLIN, String.valueOf(fromkotlin2.size()));
    }

  class JK extends ProductsK{

  }
}
// https://kotlinlang.org/docs/reference/classes.html LAST SESSION