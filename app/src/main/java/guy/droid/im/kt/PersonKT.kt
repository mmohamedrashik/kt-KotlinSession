package guy.droid.im.kt

import android.util.Log

/**
 * Created by admin on 6/27/2017.
 */
open class PersonKT : PersonABS() {
   // val KOTLIN = "KOTLIN"
    var a : Int = 0;
    var b : Int = 0;
    init {
        Log.d(KOTLIN,"INITIALIZED")
    }


    open fun Add(a : Int, b : Int) : Int
    {
        this.a = a
        this.b = b
        return a + b

    }
    fun DefaultCaller()
    {
        Log.d(KOTLIN,"Caller")
    }
    fun DetailsViewer(person : Person)
    {
        var id = person.id
        var name = person.names
        var department = person.department
        var hobs = person.hobs
        var hash = person.hashmap

        var length : Int = hobs?.size as Int
        Log.d(KOTLIN,"ID $id NAME $name DEP $department LENGTH $length")

        for (items in hobs.indices)
        {
            Log.d(KOTLIN,"ELEMENTZ $items ${hobs[items]}")
        }

        Log.d(KOTLIN,"HASH ${hash?.get("ONE")}")
    }
}